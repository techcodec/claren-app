import { ADD_ORDER_ITEM, EDIT_ORDER_ITEM, RESET_ORDERS } from './contants';

export const addOrderItemCreator = orderItem => ({
    type: ADD_ORDER_ITEM,
    payload: orderItem,
});

export const editOrderItemCreator = orderItem => ({
    type: EDIT_ORDER_ITEM,
    payload: orderItem,
});


export const resetOrdsersCreator = () => ({
    type: RESET_ORDERS,
});
