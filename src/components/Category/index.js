import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';

import './Category.scss';
import MenuItem from '../common/MenuItem';

class CategoryLayout extends PureComponent {
    static propTypes = {
        categoryId: PropTypes.string.isRequired,
        foods: PropTypes.array,
    }
    static defaultProps = {
        foods: [],
    }
    renderFoods = () => {
        const { foods, categoryId } = this.props;
        return foods.map(food => (
            <div key={food.id} className="main-foods__item">
                <MenuItem style={{ fontSize: '20px' }} title={`${food.name} $${food.price}`} src={food.imageUrl} to={`/${categoryId}/${food.id}`} />
            </div>
        ));
    }
    render() {
        return (
            <Container className="main-foods">
                {this.renderFoods()}
            </Container>
        );
    }
};

export default CategoryLayout;
