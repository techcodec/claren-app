import React from 'react';

const CheckboxGroup = ({ title, children }) => (
    <div className="checkbox-group">
        <div className="checkbox-group__title">{title}</div>
        <div className="checkbox-group__checkboxes">
            {children}
        </div>
    </div>
);

export default CheckboxGroup;
