import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './FoodCheckbox.scss';

const FoodCheckbox = ({ checked, label, onChange, count, id, changeOrderItemCount }) => (
    <div className="food__checkbox">
        <div onClick={onChange} className="food__checkbox__main">
            <div className="food__checkbox__mark">
                {checked && <i className="icon-ok-circled checked-icon" />}
                {!checked && <i className="icon-cancel-circled" />}
            </div>
            <div className="food__checkbox__label">{label}</div>
        </div> 
        <div className={cn('food__checkbox__change-count', { 'food__checkbox__change-count--active': count > 0 })}>
            <div onClick={() => {changeOrderItemCount('inc', id)}} className="food__checkbox__change-count__plus">
                <i className="icon-plus" />
            </div>
            <div onClick={() => {changeOrderItemCount('dec', id)}} className="food__checkbox__change-count__minus">
                <i className="icon-minus" />
            </div>
            <div className="food__checkbox__change-count__num">{count}</div>
        </div>
    </div>
);
FoodCheckbox.propTypes = {
    checked: PropTypes.bool,
};
FoodCheckbox.defaultProps = {
    checked: false,
};

export default FoodCheckbox;
