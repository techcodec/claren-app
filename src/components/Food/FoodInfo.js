import React from 'react';
import Col from 'react-bootstrap/Col';

import './FoodInfo.scss';

const FoodInfo = ({ imageUrl, name, description }) => (
    <Col className="food__info" md={12} lg={3}>
        <img className="food__info__img" src={imageUrl} alt="food" />
        <div className="food__info__name">{name}</div>
        <div className="food__info__description">{description}</div>
    </Col>
);

export default FoodInfo;
