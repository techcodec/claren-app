import React from 'react';

import CheckboxGroup from './CheckboxGroup';
import FoodCheckbox from './FoodCheckbox';

const FoodOptional = ({ optional, editItemId, getOrderItem, changeItemState, addOrderItem, editOrderItem, isEditPage, changeOrderItemCount, getTopingCount }) => (
    <div className="food__topings__optional">
        <div className="food__topings__optional__check-out">
            <i className="icon-basket" />
            <span>Check Out</span>
        </div>
        <div className="food__topings__optional__order" onClick={isEditPage ? () => {console.log(editItemId); editOrderItem(editItemId)} : addOrderItem}>{isEditPage ? 'Edit Item' : 'Add to order' }</div>
        <CheckboxGroup title="Add optional items">
            {optional.map(opt => (
                <FoodCheckbox
                    changeOrderItemCount={changeOrderItemCount}
                    id={opt.id}
                    count={getTopingCount(opt.id)}
                    key={opt.id}
                    onChange={() => {changeItemState(opt)}}
                    checked={getOrderItem(opt)}
                    label={`${opt.name} + $${opt.price}`}
                />
            ))}
        </CheckboxGroup>
    </div>
);

export default FoodOptional;
