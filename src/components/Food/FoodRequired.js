import React from 'react';
import { Link } from 'react-router-dom';

import CheckboxGroup from './CheckboxGroup';
import FoodCheckbox from './FoodCheckbox';

const FoodRequired = ({ cheeses, onions, getOrderItem, changeItemState, changeOrderItemCount, getTopingCount }) => (
    <div className="food__topings__required">
        <Link to={`${window.location.hash.slice(1)}?order=true`} className="food__topings__required__next">Next Page</Link>
        <CheckboxGroup title="Select cheese (select one)">
            {cheeses.map(cheese => {
                return <FoodCheckbox
                    changeOrderItemCount={changeOrderItemCount}
                    count={getTopingCount(cheese.id)}
                    key={cheese.id}
                    id={cheese.id}
                    onChange={() => {changeItemState(cheese)}}
                    checked={getOrderItem(cheese)}
                    label={`${cheese.name} + $${cheese.price}`}
                />
            })}
        </CheckboxGroup>
        <CheckboxGroup title="Select onion  (select all that apply)">
            {onions.map(onion => (
                <FoodCheckbox
                    changeOrderItemCount={changeOrderItemCount}
                    count={getTopingCount(onion.id)}
                    key={onion.id}
                    id={onion.id}
                    onChange={() => {changeItemState(onion)}}
                    checked={getOrderItem(onion)}
                    label={`${onion.name} + $${onion.price}`}
                />
            ))}
        </CheckboxGroup>
    </div>
);

export default FoodRequired;
