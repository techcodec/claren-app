import React from 'react';
import Col from 'react-bootstrap/Col';

import './FoodTopings.scss';
import FoodRequired from './FoodRequired';
import FoodOptional from './FoodOptional';

const FoodTopings = ({ cheeses, onions, optional, editItemId, isOrderPage, isEditPage, getOrderItem, addOrderItem, editOrderItem, changeItemState, changeOrderItemCount, getTopingCount }) => (
    <Col className="food__topings-wrp" md={12} lg={9}>
        <div className="food__topings">
            {!isOrderPage && (
                <FoodRequired
                    cheeses={cheeses}
                    onions={onions}
                    getOrderItem={getOrderItem}
                    changeItemState={changeItemState}
                    changeOrderItemCount={changeOrderItemCount}
                    getTopingCount={getTopingCount}
                />
            )}
            {isOrderPage && (
                <FoodOptional
                    optional={optional}
                    editItemId={editItemId}
                    isEditPage={isEditPage}
                    getOrderItem={getOrderItem}
                    changeItemState={changeItemState}
                    addOrderItem={addOrderItem}
                    editOrderItem={editOrderItem}
                    changeOrderItemCount={changeOrderItemCount}
                    getTopingCount={getTopingCount}
                />
            )}
        </div>
    </Col>
);

export default FoodTopings;
