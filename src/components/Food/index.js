import React, { PureComponent } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import './Food.scss';
import FoodInfo from './FoodInfo';
import FoodTopings from './FoodTopings';

class Food extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            orderItems: [],
        }
    }
    componentDidMount() {
        const { topings, orders, foodId, location } = this.props;
        const isEditPage = location.search.indexOf('edit=true') !== -1;
        const editItemId = isEditPage && location.search.slice(+location.search.indexOf('id=') + 3);
        const selectedTopings = topings.filter(toping => toping.selected);
        const selectedTopingsMapped = selectedTopings.map(toping => ({ ...toping, count: 1 }));
        this.setState({
            orderItems: [...this.state.orderItems, ...selectedTopingsMapped],
        });
        if(isEditPage) {
            orders.forEach(order => {
                if(order.foodId === foodId && order.id.toString() === editItemId) {
                    this.setState({
                        orderItems: order.items,
                    });
                }
            });
        }
    }
    getOrderItem = ordItem => {
        const { orderItems } = this.state;
        for (let i = 0; i < orderItems.length; i++) {
            if(orderItems[i].id === ordItem.id) {
                return true;
            }
        }
        return false;
    }
    changeItemState = ordItem => {
        const { orderItems } = this.state;
        const isChecked = this.getOrderItem(ordItem);
        let newOrdItems = [...orderItems];
        if(isChecked) {
            if(ordItem.group) {
                return;
            }
            this.setState({
                orderItems: orderItems.filter(orderItem => orderItem.id !== ordItem.id),
            });
        }
        else {
            
            if(ordItem.group) {
                newOrdItems = orderItems.filter(item => item.group !== ordItem.group);
            }
            this.setState({
                orderItems: [...newOrdItems, { ...ordItem, count: 1 }],
            });
        }
    }
    addOrderItem = () => {
        const { addOrderItem, price, foodId, categoryId, history, nameWithoutPrice } = this.props;
        const { orderItems } = this.state;
        const orderItem = {
            id: Date.now(),
            foodId,
            categoryId,
            foodName: nameWithoutPrice,
            foodPrice: price,
            items: orderItems,
        };
        addOrderItem(orderItem);
        history.push('/');
    }
    editOrderItem = id => {
        const { editOrderItem, price, foodId, categoryId, history, nameWithoutPrice } = this.props;
        const { orderItems } = this.state;
        const orderItem = {
            id,
            foodId,
            categoryId,
            foodName: nameWithoutPrice,
            foodPrice: price,
            items: orderItems,
        };
        editOrderItem(orderItem);
        history.push('/order');
    }
    changeOrderItemCount = (type, id) => {
        const { topings } = this.props;
        const { orderItems } = this.state;
        let isInc = false;
        let newItems = orderItems.map(item => {
            if(item.id === id) {
                if(type === 'inc') {
                    isInc = true;
                    return { ...item, count: item.count + 1 };
                }
                else if(type === 'dec') {
                    const newCount = (item.count - 1) <= 0 ? 1 : item.count - 1;
                    return { ...item, count: newCount  };
                }
            }
            return item;
        });
        if(!isInc && type === 'inc') {
            const toping = topings.find(toping => toping.id === id);
            const isSameGroup = orderItems.find(item => (item.group === toping.group) && item.group !== undefined );
            if(!isSameGroup) {
                newItems = [...newItems, { ...toping, count: 1 }];
            }
        }
        this.setState({
            orderItems: newItems,
        });
    }
    getTopingCount = id => {
        const { orderItems } = this.state;
        const toping = orderItems.find(item => item.id === id);
        const topingCount = toping && toping.count;
        return topingCount || (toping ? 1 : 0);
    }
    render() {
        const { imageUrl, name, description, location, topings } = this.props;
        const cheeses = topings.filter(toping => toping.category === 'cheeses');
        const onions = topings.filter(toping => toping.category === 'onions');
        const optional = topings.filter(toping => toping.category === 'optional');
        const isOrderPage = location.search.indexOf('order') !== -1;
        const isEditPage = location.search.indexOf('edit=true') !== -1;
        const editItemId = isEditPage && location.search.slice(+location.search.indexOf('id=') + 3, location.search.indexOf('?order'));
        return (
            <Container className="food">
                <Row>
                    <FoodInfo imageUrl={imageUrl} name={name} description={description} />
                    <FoodTopings
                        cheeses={cheeses}
                        onions={onions}
                        optional={optional}
                        isOrderPage={isOrderPage}
                        isEditPage={isEditPage}
                        editItemId={+editItemId}
                        getOrderItem={this.getOrderItem}
                        addOrderItem={this.addOrderItem}
                        editOrderItem={this.editOrderItem}
                        changeItemState={this.changeItemState}
                        changeOrderItemCount={this.changeOrderItemCount}
                        getTopingCount={this.getTopingCount}
                    />
                </Row>
            </Container>
        );
    }
};

export default Food;
