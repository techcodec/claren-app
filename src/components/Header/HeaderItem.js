import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const HeaderItem = ({ to, title }) => (
    <Link className="main-header__item" to={to}>
        {title}
        <i className="icon-right-circle" />
    </Link>
);
HeaderItem.propTypes = {
    to: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

export default HeaderItem;
