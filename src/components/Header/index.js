import React, { Component } from 'react';

import './Header.scss';
import HeaderItem from './HeaderItem';

class Header extends Component {
    render() {
        const { category, food } = this.props;
        return (
            <div className="main-header">
                <HeaderItem to="/" title="Full Menu" />
                {category.isAvailable && <HeaderItem to={category.path} title={category.name} />}
                {food.isAvailable && <HeaderItem to={food.path} title={food.name} />}
            </div>
        );
    }
};

export default Header;
