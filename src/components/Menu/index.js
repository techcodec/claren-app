import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Container from 'react-bootstrap/Container';

import './Menu.scss';
import MenuItem from '../common/MenuItem';

class Menu extends PureComponent {
    static propTypes = {
        categories: PropTypes.array,
    }
    static defaultProps = {
        categories: [],
    }
    renderCategories = () => {
        const { categories } = this.props;
        return categories.map(category => (
            <div key={category.id} className="menu__item-wrp">
                <MenuItem isFavorite={category.isFavorite} title={category.name} src={category.imageUrl} to={`/${category.id}`} />
            </div>
        ));
    }
    render() {
        
        return (
            <Container className="menu">
                {this.renderCategories()}
            </Container>
        );
    }
};

export default Menu;
