import React, { PureComponent } from 'react';

import './OrderMain.scss';

const PriceBlock = ({ index, foodName, foodPrice, items, editOrderItem }) => (
    <div className="order-main__price">
        <div className="order-main__price__food">
            <div onClick={editOrderItem} className="order-main__price__food__index">{`Edit Item / ${index}`}</div>
            <div className="order-main__price__food__name">{foodName}</div>
            <div className="order-main__price__food__price">${foodPrice}</div>
        </div>
        <div className="order-main__price__topings">
            {items.map((item, i) => (
                <div key={i} className="order-main__price__toping">
                    <div className="order-main__price__toping__name">{`+ ${item.name} x${item.count}`}</div>
                    <div className="order-main__price__toping__price">{`+ ${item.price * item.count}`}</div>
                </div>
            ))}
        </div>
    </div>
);

class OrderMain extends PureComponent {
    render() {
        const { orders, totalPrice, tax, editOrderItem } = this.props;
        const tPrice = +totalPrice + tax;
        return (
            <div className="order-main">
                <div className="order-main__prices">
                    {orders.map((order, i) => (
                        <PriceBlock
                            key={i}
                            index={i + 1}
                            foodName={order.foodName}
                            foodPrice={order.foodPrice}
                            items={order.items}
                            editOrderItem={() => {editOrderItem(order)}}
                        />
                    ))}
                </div>
                <div className="order-main__prices order-main__total">
                    <div className="order-main__price">
                        <div className="order-main__price__food">
                            <div className="order-main__price__food__name total">Subtotal</div>
                            <div className="order-main__price__food__price">${totalPrice}</div>
                        </div>
                        <div className="order-main__price__topings">
                            <div className="order-main__price__toping">
                                <div className="order-main__price__toping__name">{`+ tax`}</div>
                                <div className="order-main__price__toping__price">{`+ 1.21`}</div>
                            </div>
                        </div>
                    </div>
                    <div className="order-main__price">
                        <div className="order-main__price__food">
                            <div className="order-main__price__food__name total">Total</div>
                            <div className="order-main__price__food__price">${tPrice.toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default OrderMain;
