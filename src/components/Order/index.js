import React, { PureComponent } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './Order.scss';
import OrderMain from './OrderMain';

const PayNowButton = () => (
    <div className="main-order__pay-now">
        <i className="icon-basket" />
        <span>Pay Now</span>
    </div>
);

class Order extends PureComponent {
    render() {
        const { orders, totalPrice, tax, resetOrders, editOrderItem } = this.props;
        return (
            <Container className="main-order">
                <Row>
                    <Col xs={12} lg={9}>
                        <OrderMain editOrderItem={editOrderItem} orders={orders} totalPrice={totalPrice} tax={tax} />
                    </Col>
                    <Col xs={12} lg={3}>
                        <PayNowButton />
                        <div onClick={resetOrders} className="main-order__button">Full Menu</div>
                    </Col>
                </Row>
            </Container>
        );
    }
};

export default Order;

