import React from 'react';
import cn from 'classnames';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';

import './MenuItem.scss';

console.log(process.env)
const MenuItem = ({ title, src, isFavorite, to, style }) => (
    <Col xs={12} sm={12} ms={6} lg={3} style={style} className={cn('menu__item', { 'is-favorite': isFavorite })}>
        
        <Link className="menu__item__link" to={to}>
            <img className="menu__item__img" src={src} alt="" />
            {isFavorite && <img className="menu__item__star" src="./images/StarFin.png" alt="star" />}
            {title}
        </Link>
    </Col>
);

export default MenuItem;
