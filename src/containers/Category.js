import React, { Component } from 'react';
import { connect } from 'react-redux';

import CategoryLayout from '../components/Category';

class Category extends Component {
    getFoodsByCategory = () => {
        const { foods, match } = this.props;
        const { category_id } = match.params;
        return foods.filter(food => food.categoryId === category_id);
    }
    render() {
        const { match } = this.props;
        return (
            <CategoryLayout categoryId={match.params.category_id} foods={this.getFoodsByCategory()} />
        );
    }
};

const mapStateToProps = state => ({
    foods: state.foods,
});

export default connect(mapStateToProps)(Category);
