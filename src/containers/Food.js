import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { addOrderItemCreator, editOrderItemCreator } from '../actions/actionCreators';
import FoodLayout from '../components/Food';

class Food extends Component {
    getActiveFood = () => {
        const { foods, match } = this.props;
        const { food_id = null } = match.params;
        if(!food_id) return null;
        return foods.find(food => food.id === food_id);
    }
    render() {
        const { location, topings, orders, addOrderItem, editOrderItem, match, history } = this.props;
        const activeFood = this.getActiveFood();
        const { imageUrl, name, price, description } = activeFood;
        return (
            <FoodLayout
                location={location}
                imageUrl={imageUrl}
                price={price}
                name={`${name} $${price}`}
                nameWithoutPrice={name}
                description={description}
                topings={topings}
                orders={orders}
                addOrderItem={addOrderItem}
                editOrderItem={editOrderItem}
                foodId={match.params.food_id}
                categoryId={match.params.category_id}
                history={history}
            />
        );
    }
};

const mapStateToProps = state => ({
    foods: state.foods,
    topings: state.topings,
    orders: state.orders,
});

const mapDispatchToProps = dispatch => ({
    addOrderItem: bindActionCreators(addOrderItemCreator, dispatch),
    editOrderItem: bindActionCreators(editOrderItemCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Food);
