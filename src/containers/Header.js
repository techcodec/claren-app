import React, { Component } from 'react';
import { connect } from 'react-redux';

import HeaderLayout from '../components/Header';

class Header extends Component {
    getCategory = () => {
        const { categories } = this.props;
        const { category_id = null } = this.props.match.params;
        if(!category_id) return { isAvailable: false };
        const activeCategory = categories.find(category => category.id === category_id);
        return {
            isAvailable: true,
            path: `/${category_id}`,
            name: activeCategory.name,
        }
    }
    getFood = () => {
        const { foods } = this.props;
        const { category_id = null, food_id = null } = this.props.match.params;
        if(!food_id || !category_id) return { isAvailable: false };
        const activeFood = foods.find(food => food.id === food_id);
        return {
            isAvailable: true,
            path: `/${category_id}/${food_id}`,
            name: activeFood.name,
        }
    }
    render() {
        const { category_id } = this.props.match.params;
        if(category_id === 'order') return null;
        return (
            <HeaderLayout category={this.getCategory()} food={this.getFood()} />
        );
    }
};

const mapStateToProps = state => ({
    categories: state.categories,
    foods: state.foods,
});

export default connect(mapStateToProps)(Header);
