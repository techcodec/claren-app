import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './Main.scss';
import Header from './Header';
import Menu from './Menu';
import Category from './Category';
import Food from './Food';
import Order from './Order';

const CompleteOrderBtn = ({ ordersCount }) => (
    <Link to="/order" className="main__complete-order">{`${ordersCount} items  |  Complete Order`}</Link>
);

class Main extends Component {
    render() {
        const { orders } = this.props;
        return (
            <div className="main-wrp">
                <div className="main-bg" />
                <div className="main">
                    {orders.length > 0 && (
                        <Switch>
                            <Route path="/:category_id" exact render={props => {
                                const { category_id } = props.match.params;
                                if(category_id === 'order') return null;
                                return <CompleteOrderBtn ordersCount={orders.length} />
                            }} />
                            <Route path="/" exact render={props => {
                                console.log(props.match)
                                return <CompleteOrderBtn ordersCount={orders.length} />
                            }} />
                        </Switch>
                    )}
                    <Switch>
                        <Route path="/:category_id/:food_id" component={Header} />
                        <Route path="/:category_id" component={Header} />
                        <Route path="/" component={Header} />
                    </Switch>
                    <Switch>
                        <Route path="/" exact component={Menu} />
                        <Route path="/order" exact component={Order} />
                        <Route path="/:category_id" exact component={Category} />
                        <Route path="/:category_id/:food_id" exact component={Food} />
                    </Switch>
                </div>
            </div>
        );
    }
};

const mapStateToProps = state => ({
    orders: state.orders,
});

export default connect(mapStateToProps)(Main);
