import React, { Component } from 'react';
import { connect } from 'react-redux';

import MenuLayout from '../components/Menu';

class Menu extends Component {
    render() {
        const { categories } = this.props;
        return (
            <MenuLayout categories={categories} />
        );
    }
};

const mapStateToProps = state => ({
    categories: state.categories,
});


export default connect(mapStateToProps)(Menu);
