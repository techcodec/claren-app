import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { resetOrdsersCreator } from '../actions/actionCreators';
import OrderLayout from '../components/Order';

class Order extends Component {
    componentWillMount() {
        const { orders, history } = this.props;
        if(orders.length === 0) {
            history.push('/');
        }
    }
    calcTotalPrice = () => {
        const { orders } = this.props;
        let price = 0;
        for(let i = 0; i < orders.length; i++) {
            price += orders[i].foodPrice;
            for(let j = 0; j < orders[i].items.length; j++) {
                price += orders[i].items[j].price * orders[i].items[j].count;
            }
        };
        return price.toFixed(2);
    }
    resetOrders = () => {
        const { resetOrders, history } = this.props;
        resetOrders();
        history.push('/');
    }
    editOrderItem = item => {
        const { history } = this.props;
        const { foodId, categoryId, id } = item;
        history.push(`/${categoryId}/${foodId}?edit=true&id=${id}`);
    }
    render() {
        const { orders } = this.props;
        return (
            <OrderLayout
                orders={orders}
                totalPrice={this.calcTotalPrice()}
                tax={1.21}
                resetOrders={this.resetOrders}
                editOrderItem={this.editOrderItem}
            />
        );
    }
};

const mapStateToProps = state => ({
    orders: state.orders,
});

const mapDispatchToProps = dispatch => ({
    resetOrders: bindActionCreators(resetOrdsersCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Order);