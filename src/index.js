import 'react-bootstrap/dist/react-bootstrap.min.js';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import { HashRouter as Router } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store/configureStore';

ReactDOM.render(
    <Router>
        <Provider store={store}>
            <App />
        </Provider>
    </Router>,
    document.getElementById('root')
);

serviceWorker.unregister();
