import { ADD_ORDER_ITEM, EDIT_ORDER_ITEM, RESET_ORDERS } from '../actions/contants';

const initialState = {
    categories: [
        { id: '1', name: 'Dogs', isFavorite: false, imageUrl: "./images/hot-dogs.png" },
        { id: '2', name: 'Burgers', isFavorite: true, imageUrl: "./images/burgers.png" },
        { id: '3', name: 'Tacos', isFavorite: false, imageUrl: "./images/tacos.png" },
        { id: '4', name: 'Pizza', isFavorite: false, imageUrl: "./images/pizzas.png" },
        { id: '5', name: 'Sides', isFavorite: false, imageUrl: "./images/sides.png" },
        { id: '6', name: 'Drinks', isFavorite: false, imageUrl: "./images/drinks.png" }
    ],
    foods: [
        { id: '11', categoryId: '2', name: 'Cheese Burger', imageUrl: "./images/burgers.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '12', categoryId: '2', name: 'Red Burger', imageUrl: "./images/burgers.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '13', categoryId: '2', name: 'Blue Burger', imageUrl: "./images/burgers.png", price: 8.21, description: "Description text" },
        { id: '14', categoryId: '2', name: 'Cheese Burger', imageUrl: "./images/burgers.png", price: 8.21, description: "Description text" },
        { id: '15', categoryId: '2', name: 'Cheese Burger', imageUrl: "./images/burgers.png", price: 8.21, description: "Description text" },
        { id: '16', categoryId: '1', name: 'Hot Dog', imageUrl: "./images/hot-dogs.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '17', categoryId: '1', name: 'Cold Dog', imageUrl: "./images/hot-dogs.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '18', categoryId: '1', name: 'Green Dog', imageUrl: "./images/hot-dogs.png", price: 8.21, description: "Description text" },
        { id: '19', categoryId: '1', name: 'Yellow Dog', imageUrl: "./images/hot-dogs.png", price: 8.21, description: "Description text" },
        { id: '50', categoryId: '1', name: 'Dog', imageUrl: "./images/hot-dogs.png", price: 8.21, description: "Description text" },
        { id: '51', categoryId: '3', name: 'Red Tacos', imageUrl: "./images/tacos.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '52', categoryId: '3', name: 'Blue Tacos', imageUrl: "./images/tacos.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '53', categoryId: '3', name: 'Tacos', imageUrl: "./images/tacos.png", price: 8.21, description: "Description text" },
        { id: '54', categoryId: '4', name: 'Pepperoni', imageUrl: "./images/pizzas.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '55', categoryId: '4', name: 'Cheese Pizza', imageUrl: "./images/pizzas.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '56', categoryId: '4', name: 'Pizza', imageUrl: "./images/pizzas.png", price: 8.21, description: "Description text" },
        { id: '57', categoryId: '4', name: 'Pizza', imageUrl: "./images/pizzas.png", price: 8.21, description: "Description text" },
        { id: '58', categoryId: '6', name: 'Coctail', imageUrl: "./images/drinks.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '59', categoryId: '6', name: 'Coffee', imageUrl: "./images/drinks.png", price: 8.21, description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'." },
        { id: '60', categoryId: '6', name: 'Drink', imageUrl: "./images/drinks.png", price: 8.21, description: "Description text" },
        { id: '61', categoryId: '6', name: 'Drink', imageUrl: "./images/drinks.png", price: 8.21, description: "Description text" },
        { id: '62', categoryId: '5', name: 'Sides 1', imageUrl: "./images/sides.png", price: 8.21, description: "Description text" },
        { id: '63', categoryId: '5', name: 'Sides 2', imageUrl: "./images/sides.png", price: 8.21, description: "Description text" },
    ],
    topings: [
        { id: '21', category: 'cheeses', selected: true, group: '1', name: 'Pepper Jack', stateName: 'pepperJack', price: 1.23 },
        { id: '22', category: 'cheeses', group: '1', name: 'Cheddar', stateName: 'cheddar', price: 1.23 },
        { id: '23', category: 'cheeses', group: '1', name: 'Swiss', stateName: 'swiss', price: 2.05 },
        { id: '24', category: 'cheeses', group: '1', name: 'No cheese', stateName: 'noCheese', price: 0 },
        { id: '31', category: 'onions', name: 'Red onion', stateName: 'redOnion', price: 0 },
        { id: '32', category: 'onions', name: 'White onion', stateName: 'whiteOnion', price: 0 },
        { id: '41', category: 'optional', name: 'Fries', stateName: 'fries',  price: 1.23 },
        { id: '42', category: 'optional', name: 'Pickle', stateName: 'pickle',  price: 1.23 },
        { id: '43', category: 'optional', name: 'Side of ketchup', stateName: 'ketchup', price: 1.23 },
        { id: '44', category: 'optional', name: 'Extra peppers',  stateName: 'peppers', price: 1.23 },
    ],
    orders: [],
};

const rootReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_ORDER_ITEM: {
            return { ...state, orders: state.orders.concat(action.payload) };
        }
        case EDIT_ORDER_ITEM: {
            return { ...state, orders: state.orders.map(order => {
                if(order.foodId === action.payload.foodId && order.id === action.payload.id) {
                    return action.payload;
                }
                return order;
            }) }
        }
        case RESET_ORDERS: {
            return { ...state, orders: [] };
        }
        default: {
            return state;
        }
    }
};

export default rootReducer;
